let promise1 = new Promise(function(resolve, reject) {
    setTimeout(() => {        
        console.log("After 3 seconds - from the promise 1"); 
        resolve("After 3 seconds - msg from promise to then");
    }, 
    3000);
});
let promise2 = new Promise(function(resolve, reject) {
    setTimeout(() => {        
        console.log("After 1 seconds - from the promise 2"); 
        reject("After 1 seconds - msg from promise to then");
    }, 
    1000);
});
Promise.all([promise1, promise2]).then(
  function(result) { console.log("All promises fulfilled."); }
).catch(
    function(error) { 
        console.log("At least one promise got rejected!");
    }
);
console.log("Program continues with other tasks...");