/*
Zapis uživo demonstracije 13.5.22
Istovjetna demonstracija postoji u .mp4 "async podsjetnik" datotekama iz prve sezone predavanja (Teams)

GEN #1

let get =  (num, cb) => {
  setTimeout(() => {
      if (1 === 2) {
        cb(new Error(), undefined);  
      } else {
        cb(undefined, num * 2)
      }
  }, 1000);  
}


get (13, function (err, res1) {
  if (err) {
    console.log("bila greska");
  } else {
    get(14, function (err, res2) {
      if (err) {
        console.log("bila greska");
      } else {
          console.log(res1 + res2);
      }
    })  
  }
})
*/

// GEN #2
let makePromise = function (num) {
    return new Promise(function (res, rej) {
        setTimeout(() => {
            if (1 === 2) {
                rej(new Error());
            } else {
                res(num * 2)
            }
        }, 500);
    });
}

/*
let total = 0;
console.log(total);
makePromise(13)
.then(function(res){
  total += res;
  console.log(total);
  return makePromise(14);
})
.then(function(res){
  total += res;
  console.log(total);
  return makePromise(15);
})
.then(function(res){
  total += res;
  console.log(total);
  return total;
})
console.log("zadnja linija", total);
*/


// GEN #3

// pogledajte IIFE

let af = async function () {
    let res = await makePromise(13);
    console.log("await", res);
    res += await makePromise(14);
    console.log("await", res);
    res += await makePromise(15);
    console.log("zadnja linija await", res);
}


af();