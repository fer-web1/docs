function Animal(name) {
  let animal = Object.create(Animal.prototype);
  animal.name = name;
  return animal;
}

Animal.prototype.eat = function(){
  console.log(`${this.name} is eating.`);
};

const cat = Animal('Cat');
const dog = Animal('Dog');
cat.eat();
dog.eat();