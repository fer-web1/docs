function Animal(name) {
  let animal = Object.create(Animal.prototype);
  animal.name = name;
  return animal;
}
Animal.prototype.eat = function(){
      console.log(`${this.name} is eating.`);
    };
const cat = Animal('Cat');
const dog = Animal('Dog');
cat.eat();
dog.eat();

function AnimalNew(name) {
  this.name = name;
}
Animal.prototype.eat = function(){
  console.log(`${this.name} is eating.`);
};
const catNew = new AnimalNew('Cat');
const dogNew = new AnimalNew('Dog');
cat.eat();
dog.eat();

class AnimalClass {
  constructor(name){
  this.name = name;
  }
  eat(){
    console.log(`${this.name} is eating.`);
  };
}
const catClass = new AnimalClass('Cat');
const dogClass = new AnimalClass('Dog');
cat.eat();
dog.eat();
