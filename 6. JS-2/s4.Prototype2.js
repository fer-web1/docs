const methods = {
  eat(){
    console.log(`${this.name} is eating.`);
  }
}

function Animal (name) {
  let animal = Object.create(methods);
  animal.name = name;
  return animal;
}

const cat = Animal('Cat');
const dog = Animal('Dog');
cat.eat();
dog.eat();