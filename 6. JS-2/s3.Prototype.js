function Animal (name) {
  let animal = {};
  animal.name = name;

  animal.eat = function () {
    console.log(`${this.name} is eating.`);
  }

  return animal;
}

const cat = Animal('Cat');
const dog = Animal('Dog');
cat.eat();
dog.eat();